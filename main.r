# Importem dades
l_data <- read.table("clipboard", dec=".")
data <- unlist(l_data)

# Anàlisis de normalitat
summary(data)
var(data)
sd(data)
plot(data)
barplot(data)

qqnorm(data)
qqline(data)
hist(data)

qqnorm(log(data))
qqline(log(data))
hist(log(data))

# PH i IC d'una mostra
t.test(log(data), mu=log(0.05))

# PH i IC de les dues mostres i anàlisis de normalitat
data_apar <- log(data2) - log(data)
qqnorm(data_apar)
qqline(data_apar)
hist(data_apar)

t.test(data_apar, mu=0)